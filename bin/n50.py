#!/usr/bin/python2.7

import sys
import os
from fasta import iterate_fasta as parse_fasta

def gc(sequence):
    count = 0
    for base in sequence:
        base = base.lower()
        if base == 'g' or base == 'c':
            count += 1
    return float(float(count) / float(len(sequence))) * float(100)

def n50(fasta):
	length_list = []
        sequences = []
	for sequence in parse_fasta(fasta):
		length_list.append(float(len(sequence[1])))
                sequences.append(sequence[1])
	length_list.sort()
	length_list.reverse()
	total = float(sum(length_list))
	n = total * float(0.50)
	n50_value = running = length_list[0]
	for length in length_list:
		if running >= n:
			return n50_value, total, \
                                len(length_list), gc(''.join(sequences))
		else:
			n50_value = length
			running += n50_value

if __name__ == '__main__':
	if len(sys.argv) != 2:
		print 'please specify fasta or - for stdin'
		exit()
	fasta = sys.argv[1]
	if fasta == '-':
		fasta = sys.stdin
	else:
		fasta = open(fasta)
	n50_value, total, contigs, gc = n50(fasta)
        print '# fasta: %s' % (fasta.name)
        print 'number of contigs: %s' % (contigs)
        print 'number of bases: %s' % ('{:,}'.format(int(total)))
        print 'gc: %.2f' % (round(gc, 2))
        print 'n50: %s' % ('{:,}'.format(int(n50_value)))
