#!/usr/bin/python2.7

'''
script for merging separate fastq files into 
an interleaved fastq file

Chris Brown (ctb@berkeley.edu)
'''

import sys
import os
import itertools

def fq_merge(R1, R2):
    """
    merge separate fastq files
    """
    c = itertools.cycle([1, 2, 3, 4])
    for r1, r2 in itertools.izip(R1, R2):
        n = c.next()
        if n == 1:
            pair = [[], []]
        pair[0].append(r1.strip())
        pair[1].append(r2.strip())
        if n == 4:
            yield pair

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'usage: fastq_merge.py <R1.fastq> <R2.fastq>'
        exit()
    R1, R2 = open(sys.argv[1]), open(sys.argv[2])
    for pair in fq_merge(R1, R2):
        print '\n'.join(itertools.chain(*pair))
